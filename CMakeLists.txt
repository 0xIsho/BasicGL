cmake_minimum_required(VERSION 3.9)

project(BasicGL LANGUAGES C CXX)

set(SOURCES
	"src/main.cpp"
)

find_package(GLFW3 REQUIRED)

add_subdirectory(vendor)
add_executable(BasicGL ${SOURCES})

target_link_libraries(BasicGL
	PRIVATE
		glfw
		glad
)
target_include_directories(BasicGL
	PRIVATE
		"src/"
)
